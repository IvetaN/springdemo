package com.example.demo.ex8;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class RandomBooleanController {

    private final RandomBooleanProvider randomBooleanProvider;

    @GetMapping("/api/random-boolean")
    public Boolean showRandomBoolean() {
        return randomBooleanProvider.getValue();
    }
}
