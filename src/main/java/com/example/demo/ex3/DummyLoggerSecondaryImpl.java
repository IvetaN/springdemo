package com.example.demo.ex3;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component("dummyLoggerSecondaryImpl")
public class DummyLoggerSecondaryImpl implements DummyLogger{

    @Override
    public void sayHello() {
        log.info("Hello from secondary!");
    }
}
