package com.example.demo.ex3;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Primary
public class DummyLoggerMainImpl implements DummyLogger {
    @Override
    public void sayHello() {
        log.info("Hello from main!");
    }
}
