package com.example.demo.ex3;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class DummyLoggerHostSecondary implements CommandLineRunner {

    private final DummyLogger dummyLogger;

    public DummyLoggerHostSecondary(
            @Qualifier("dummyLoggerSecondaryImpl") DummyLogger dummyLogger) {
        this.dummyLogger = dummyLogger;
    }

    @Override
    public void run(String... args) throws Exception {
        dummyLogger.sayHello();
    }

}
