package com.example.demo.ex9.controller;

import com.example.demo.ex9.dto.FileDataWrap;
import com.example.demo.ex9.exception.SdaException;
import com.example.demo.ex9.model.FileData;
import com.example.demo.ex9.service.FileDataService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class FileController {

    private final FileDataService fileDataService;

    @GetMapping("/api/files-data")
    public FileDataWrap getFileData() {
        return fileDataService.getAllFileData();
    }

    @GetMapping("/api/files-data/{id}")
    public FileData getFileDataById(@PathVariable("id") String id) throws SdaException {
        return fileDataService.getFileDataById(id);
    }

    @PostMapping("/api/files-data")
    public ResponseEntity createFileData(@RequestBody FileData fileData){
        UUID createdFileDataId = fileDataService.save(fileData);
        return ResponseEntity
                .created(java.net.URI.create("localhost:8080/api/files-data/"+createdFileDataId.toString()))
                .build();
    }

    @PutMapping("/api/files-data/{id}")
    public ResponseEntity updateFileData(@PathVariable("id") String id, @RequestBody FileData fileData){
        fileDataService.update(id, fileData);
        return ResponseEntity.accepted().build();
    }

    @DeleteMapping("/api/files-data/{id}")
    public ResponseEntity deleteFileData(@PathVariable("id") String id){
        fileDataService.delete(id);
        return ResponseEntity.accepted().build();
    }



}
