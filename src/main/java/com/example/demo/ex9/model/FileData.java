package com.example.demo.ex9.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@With
@Entity(name="files_data")
public class FileData {

    @Id
    @GeneratedValue
    private UUID id;
    private String fileName;
    private String extension;
    private Integer sizeInKb;
    private String content;
}
