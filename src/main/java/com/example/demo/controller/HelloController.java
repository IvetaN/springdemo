package com.example.demo.controller;

import com.example.demo.ex4.beans.StringUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import java.util.List;

//@Profile("demo")
@RestController
@RequiredArgsConstructor
@RequestMapping("/welcome")
public class HelloController {

    private final StringUtil stringUtil;
    private final PersonRepository personRepository;

    @GetMapping("/hello/{name}")
    public String sayHello(@PathVariable("name") String name) {
        return stringUtil.formSentence(List.of("Hello",name,"from", "controller"));
    }

    @GetMapping("/bye")
    public String sayBye() {
        return "Bye";
    }

    @GetMapping("/all")
    public List<Person> getAll() {
        return personRepository.findAll();
    }


    @PostMapping("/save")
    @ResponseStatus(HttpStatus.CREATED)
    public String saveName(@Valid @RequestBody Person person) {
        personRepository.save(person);
        return "Ok" + person.getName();
    }

    @PutMapping("/update/{name}")
    public String update(
            @PathVariable("name") String name,
            @RequestBody Person person) {
        List<Person> foundPersons = personRepository.findAllByName(name);
        if (foundPersons.size() > 1 || foundPersons.size() < 1) {
            return "Found more than one or none, can't update";
        }
        person.setId(foundPersons.get(0).getId());
       personRepository.save(person);
        return "Ok" + person.getName();
    }

    @DeleteMapping("/delete/{name}")
    public String delete(@PathVariable("name") String name){
        List<Person> foundPersons = personRepository.findAllByName(name);
        if (foundPersons.size() > 1 || foundPersons.size() < 1) {
            return "Found more than one or none, can't delete";
        }
        personRepository.delete(foundPersons.get(0));
        return "Removed" + name;
    }
}

