package com.example.demo.controller;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Min;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name="persons")
public class Person {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int id;
    @Length(max=50, message = "Name can't be longer than 50 chars.")
    private String name;
    @Min(100)
    private int age;
}
