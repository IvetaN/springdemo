package com.example.demo.ex1;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class DummyLogger implements CommandLineRunner {
    @Override
    public void run(String... args) throws Exception {
        System.out.println("Hello from task1");

    }

    public void sayHello(String text) {
        log.info(text);
    }
}
